class ReHelper(object):
	"""docstring for ReHelper"""
	helpers={}
	def __init__(self):
		super(ReHelper, self).__init__()
		self.helpers.update({"comienza con":self.startswith,
			"termina con":self.endswith,
			"seguido de ":self.nextElement,
			"las opciones":self.options,
			"el elemento":self.nextElement,
			"es opcional":self.is_optional,
			"debe ser uno o mas":self.one_more,
			})
	def startswith(self,string):
		"""
		"""
	def endswith(self,string):
		"""
		"""
	def is_optional(self,element):
		"""
		"""
	def one_more(self,element):
		"""
		"""

	def group(self,group):
		"""
		"""
	def options(self,*options):
		"""
		"""
	def nextElement(self,string):
		"""
		"""
	def gethelper(self,string):
		"""
		"""
	def evaluate(self,string,expression):
		import re
		_expression=""
		c=0
		serached=""
		try:
			while re.findall(_expression,string):
				searched=re.findall(_expression,string)
				if expression[c]=="\\":
					_expression+=expression[c]+expression[c+1]
					c+=1
				else:
					_expression+=expression[c]
				c+=1
		except:
			pass

		return [searched,_expression]
	def getrexpression(self,string,startswith,endswith):
		pass

	def getrexpression2(self,string,ini=0,end=None,limiters=[],optionals=[],required=[]):
		"""
		genera un patron de expresiones regulares que concuerda con el caso actual

		ejemplo:
		>>>string="hola mundo"
		>>>getexresion(string)
		\w+\s\w+

		"""
		import re
		currentRegex=None
		expression=[]
		symbols=["\w","[0-9]","\(","\)","\[","\]","\s","'",'"',"\$"]
		temp=""
		for x in string:
			for elem in symbols:
				if re.findall(elem,x):
					if currentRegex!=None:
						if currentRegex!=elem+"+" and currentRegex==elem:				
							currentRegex=elem+"+"	
							expression[-1]+="+"
							break	
						elif currentRegex!=elem:

							if currentRegex!=elem+"+":
								expression.append(elem)
								currentRegex=elem	
							break
					else:
						currentRegex=elem
						expression.append(elem)
						break
			else:
				currentRegex=elem
				expression+=x
			if x.strip()=="":
				temp=""
			else:
				temp+=x
			if expression:
				for elem in required:
					if temp==elem:
						expression[-1]=elem
						temp=""


		return expression
	def replace_outstring(self,sub,replace,expression):
		"""
		"""
		openstr=""
		_sub=""
		_expression=expression
		for k,elem in enumerate(_expression):
			if openstr and elem==openstr:
				openstr=""
			elif not openstr and (elem=="'" or elem=='"'):
				openstr=elem
			else:
				if elem==sub[0]:
					_sub+=elem
				elif _sub:
					if _sub+elem in sub:
						_sub+=elem
					else:
						_expressionn[k]=replace
						_sub=""
		return _expression
	def replace_expression(self,sub,replace,expression):
		_expression=expression
		for k,elem in enumerate(_expression):
			if elem==sub:
				_expression[k]="("+_expression[k]+")"
		return _expression
	def replace_onstring(self,sub,replace,expression,string):
		_expression=expression
		for k,elem in enumerate(_expression):
			if elem==sub:
				_expression[k]="("+_expression[k]+")"
		return re.sub("".join(_expression),replace,string)







	def mergeexpressions(self,string,*expressions):
		expresion=[]
		for ex in expressions:
			for k,token in enumerate(ex):
				options=[]
				current=None
				for ex2 in expressions:
					if ex!=ex2:
						if token==ex2[k]:
							if current==None:
								current=token
							else:
								if current!=token:
									options.append(token)
								current=token
						else:
							options.append(ex2[k])






if __name__=="__main__":
	import re
	helper=ReHelper()
	string="if ( ! empty( $GLOBALS['content_width'] ) )"
	expression=helper.getrexpression2(string,required=["if"])
	#print helper.evaluate(string,expression)
	#print expression
	print "Es correcto? :", "si" if re.findall("("+"".join(expression)+")",string) else "no"


		